/**
 * api.js
 *
 * Gets data from the UCahoot API.
 */

let axios = require('axios'),
  config = require('../config');

let currentPath = '';
let currentAuthToken = '';

function makeRequest(method, params, data) {
  let options = {
    method: method.toLowerCase(),
    params: params || {},
    data: data || {},
    headers: {
      Accepts: 'application/json'
    }
  };

  options.url = config.apiUrl + currentPath;

  if (currentAuthToken) {
    options.headers.Authorization = 'Bearer ' + currentAuthToken;
  }
  if (data) {
    options.headers['Content-Type'] = 'application/json';
  }

  // clear out cached values
  currentPath = '';
  currentAuthToken = '';

  return axios(options)
  .then((response) => {
    // only want the actual API server response
    return response.data;
  });
}

function API() {
  this.path = (...strs) => {
    for (let i=0; i<strs.length; i++) {
      let str = strs[i];

      if (!str) {
        throw new TypeError('Attempted to call path() on falsy object');
      }

      if (str.charAt(0) !== '/') {
        str = '/' + str;
      }

      currentPath += str;
    }

    return this;
  };

  this.setAuthToken = (token) => {
    currentAuthToken = token;

    return this;
  };

  this.get = (params) => {
    return makeRequest('get', params);
  };

  this.post = (data) => {
    return makeRequest('post', null, data);
  };

  this.put = (data) => {
    return makeRequest('put', null, data);
  };

  this.delete = (data) => {
    return makeRequest('delete', null, data);
  };
}

module.exports = new API();
