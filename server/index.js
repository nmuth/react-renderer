let express = require('express'),
	aws = require('aws-sdk'),
	config = require('./config'),
	responder = require('./modules/responder');

// configure AWS
aws.config.region = config.amazon.region;
aws.config.accessKeyId = config.amazon.accessKeyId;
aws.config.secretAccessKey = config.amazon.secretAccessKey;

let app = express();

app.use(responder);

// authentication middleware-- reject if client does not provide a valid access
// token
app.use(function(req, res, next) {
	let token = req.headers.Authorization || req.headers.authorization,
		queryToken = req.query.authToken;

	token = token || queryToken;

	if (token) {
		req.authToken = token.replace(/Bearer\s/, '');

		return next();
	} else {
		return res.respond(400);
	}
});

require('./routes')(app);

app.listen(config.port);
console.info(`Magic happens on port ${config.port}`);
