/**
 * url-params.js
 *
 * Defines URL param handlers for the app.
 */

let api = require('../../modules/api');

function getCahoot(req, res, next, id) {
  api
  .setAuthToken(req.authToken)
  .path('cahoot', id)
  .get()
  .then((response) => {
    req.cahoot = response.content[0];

    if (req.cahoot) {
      return next();
    } else {
      return res.respond(404);
    }
  })
  .catch((err) => {
    console.error(err.stack);

    return res.respond(500);
  });
}

module.exports = function(app) {
  app.param('cahootId', getCahoot);
};
