/**
 * cahoot-pdf/services.js
 *
 * Defines API services for rendering a cahoot PDF.
 */
let fs = require('fs'),
  path = require('path'),
  exec = require('child_process').exec,
  uuid = require('uuid').v4,
  htmlPdf = require('html-pdf'),
  aws = require('aws-sdk'),
  React = require('react'),
  PagePreviewClass = require('../../../src/PagePreview.react'),
  api = require('../../modules/api'),
  config = require('../../config');

let PagePreview = React.createFactory(PagePreviewClass);

let s3 = new aws.S3({
  params: {
    Bucket: config.amazon.bucket,
    ACL: 'public-read',
    ContentType: 'application/pdf'
  }
});

function getPages(cahoot, authToken) {
  return api.setAuthToken(authToken)
  .path(`cahoot/${cahoot.CahootID}/book/${cahoot.MetadataID}/page`)
  .get()
  .then((response) => {
    return response.content;
  })
  .then((pages) => {
    return Promise.all(pages.map((page) => {
      return api.setAuthToken(authToken)
      .path('pictureBookContent')
      .get({
        BookID: cahoot.MetadataID,
        PageNumber: page.PageNumber,
        getLeaf: 1
      })
      .then((response) => {
        page.content = response;

        return page;
      });
    }));
  })
  .then((pages) => {
    return pages.map((page) => {
      // transform each page into HTML with React
      let html = React.renderToString(PagePreview({page: page}));

      page.html = html;

      return page;
    });
  });
}

function savePdf(cahoot, pages) {
  return Promise.all(pages.map((page) => {
    return new Promise((resolve, reject) => {
      let filename = path.join(config.tmpDir,
          `book${page.BookID}-page${page.PageNumber}`) + '.pdf';

      console.log('creating', filename);

      htmlPdf.create(page.html, {
        height: '11.7in',
        width: '16.54in'
      }).toFile(filename, (err, result) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(result.filename);
        }
      });
    });
  }))
  .then((filenames) => {
    return new Promise((resolve, reject) => {
      let outputFilename = path.join(config.tmpDir,
          `cahoot${cahoot.CahootID}.pdf`),
        command = `pdftk ${filenames.join(' ')} output ${outputFilename}`;

      console.warn(`Executing command: '${command}'`);

      exec(command, function(err, stdout, stderr) {
        if (err) {
          return reject(err);
        } else {
          return resolve(outputFilename);
        }
      });
    });
  })
  .then((filename) => {
    // upload file to Amazon S3
    let Key = `pdf/${uuid()}.pdf`,
      Body = fs.createReadStream(filename);

    return new Promise((resolve, reject) => {
      s3.upload({ Key, Body })
      .on('httpUploadProgress', function(evt) {
        let progress = ((evt.loaded / evt.total) * 100).toFixed(0) + '%';

        console.log(`uploading to S3 (${progress} complete)`);
      })
      .send((err, data) => {
        if (err) {
          return reject(err);
        } else {
          let publicUrl = config.amazon.cdnUrl + '/' + Key;

          return resolve(publicUrl);
        }
      });
    });
  });
}

module.exports.renderPdf = function renderPdf(req, res) {
  if (req.cahoot.Type === 'PictureBook') {
    getPages(req.cahoot, req.authToken)
    .then((pages) => {
      if (req.query.html && req.query.page) {
        return res.send(pages[req.query.page].html);
      } else {
        savePdf(req.cahoot, pages)
        .then((publicUrl) => {
          return res.respond({
            content: {
              AssetUrl: publicUrl
            }
          });
        });
      }
    })
    .catch((err) => {
      console.error(err.stack);

      return res.respond(500);
    });
  } else {
    return res.respond({
      status: 400,
      error: 'Only PictureBook cahoots can produce PDFs.'
    });
  }
};
