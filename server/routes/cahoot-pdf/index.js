/**
 * cahoot-pdf/index.js
 *
 * Exposes API endpoint to render a PDF from a given cahoot, upload the PDF
 * to Amazon S3, and return the Cloudfront access URL for the PDF object.
 */

let services = require('./services');

module.exports = function(app) {
  app.route('/cahoot/:cahootId/pdf')
    .get(services.renderPdf);
};
