/**
 * routes/index.js
 *
 * Defines API endpoints for the compute server.
 */

module.exports = (app) => {
  require('./url-params')(app);
  require('./cahoot-pdf')(app);
};
