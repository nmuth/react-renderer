// transform JSX/ES6 into Node-compatible ES5
// but don't do this for node_modules
var babel = require('babel-core'),
	fs = require('fs');

require.extensions['.js'] = function(module, filename) {
	var src = fs.readFileSync(filename, { encoding: 'utf8' }),
		isNodeModule = filename.match(/node_modules/);
	
	if (isNodeModule === null) {
		// run babel on this if it is not a node module
		src = babel.transform(src).code;
	}

	return module._compile(src, filename);
};

// start the app
require('./server');

// load some npm module
require('react');

