/**
 * ContentItem.react.js
 *
 * Renders a UCahoot content item which will be provided in props.
 */

let React = require('react');

let ContentItem = React.createClass({
	propTypes: {
		contentItem: React.PropTypes.object.isRequired
	},

	getStyle() {
		let content = this.props.contentItem;

		let style = {
			position: 'absolute',
			width: content.width + 'px',
			height: content.height + 'px',
			left: content.XOff + 'px',
			top: content.YOff + 'px',
			zIndex: content.ZOff
		};

		if (content.font) {
			style.fontFamily = '\'' + content.font.FontFamily + '\'';
		}

		return style;
	},

	getContentContainerStyle() {
		return {
			overflow: 'hidden',
			width: '100%',
			height: '100%',
			backgroundColor: 'transparent'
		};
	},

	getHtml(content) {
		let val = {
			__html: content.contentData
		};

		return val;
	},

	render() {
		let content = this.props.contentItem;

		return (
			<div className="content-item" style={this.getStyle()}>
				<div className="content-container"
						style={this.getContentContainerStyle()}>
					{
						content.contentType === 'image'? (
							<img src={content.contentData}
									style={{width: '100%', height: '100%'}}/>
						)
						: content.contentType === 'text'? (
							<span dangerouslySetInnerHTML={this.getHtml(content)}></span>
						)
						: null
					}
				</div>
			</div>
		);
	}
});

module.exports = ContentItem;
