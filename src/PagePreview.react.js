/**
 * PagePreview.react.js
 *
 * Defines a preview for a page of a cahoot.
 */

let React = require('react'),
  ContentItem = require('./ContentItem.react');

class PagePreview extends React.Component {
  constructor() {
    super();

    console.info('creating preview');
  }

  getPagePreviewStyle() {
    return {
      height: '11.7in',
      width: '16.54in',
      position: 'relative'
    };
  }

  render() {
    return (
      <div className="page-preview" style={this.getPagePreviewStyle()}>
        <style>
          {`
            // default fonts
            @font-face {
              font-family: \'OpenSansLight\';
              src: url('/css/fonts/OpenSans-Light.ttf');
            }

            @font-face {
              font-family: 'OpenSansRegular';
              src: url('/css/fonts/OpenSans-Regular.ttf');
            }

            body {
              background: #fff;
              font-family: 'OpenSansLight';
              font-size: 16px;
              margin: 0px;
            }

            h1, h2, h3, h4 {
              font-family: 'OpenSansRegular';
            }
          `}
        </style>
        {
          this.props.page.content.map((content) => {
            return <ContentItem contentItem={content}/>;
          })
        }
      </div>
    );
  }
}

PagePreview.propTypes = {
  page: React.PropTypes.object.isRequired
};

module.exports = PagePreview;
